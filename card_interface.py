from abc import ABC, abstractmethod


class CardInterface(ABC):

    @abstractmethod
    def string(self) -> str:
        pass

    @abstractmethod
    def is_ge(self, other: 'CardInterface') -> bool:
        pass

    @abstractmethod
    def _value_valid(self, value: object) -> bool:
        pass

    @abstractmethod
    def get_value(self) -> object:
        pass

    @abstractmethod
    def comparable(self, other: 'CardInterface') -> bool:
        pass

    @abstractmethod
    def __lt__(self, other: 'CardInterface') -> bool:
        pass

    @abstractmethod
    def __eq__(self, other: object) -> bool:
        pass

    def __le__(self, other: 'CardInterface') -> bool:
        return self.__lt__(other) or self.__eq__(other)

    @abstractmethod
    def __ge__(self, other: 'CardInterface') -> bool:
        pass

    @abstractmethod
    def __str__(self):
        pass
