from typing import List
from abc import ABC, abstractmethod
from game_state import GameState


class GameServerInterface(ABC):

    @abstractmethod
    def play(self, player: int, card_num: int) -> bool:
        pass

    @abstractmethod
    def state(self, player: int) -> GameState:
        pass

    @abstractmethod
    def game_winners(self) -> List[int]:
        pass
