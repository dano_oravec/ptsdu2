from typing import Tuple
from abc import ABC, abstractmethod
from card_interface import CardInterface


class TrickInterface(ABC):

    @abstractmethod
    def play_as_lowest(self, c: CardInterface, curr_hand_cards: Tuple[CardInterface, ...]) -> bool:
        pass

    @abstractmethod
    def play_normal(self, c: CardInterface) -> bool:
        pass

    @abstractmethod
    def winner(self, trick_started_by: int, num_players: int) -> int:
        pass

    @abstractmethod
    def size(self) -> int:
        pass

    @abstractmethod
    def cards(self) -> Tuple[CardInterface, ...]:
        pass

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def is_finished(self, num_players: int) -> bool:
        pass

