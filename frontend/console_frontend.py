from game_server import GameServer
from game_server_interface import GameServerInterface


class ConsoleFrontend:

    def __init__(self):
        self._game_server: GameServerInterface = None
        self._player = 0
        self._num_cards = 6
        self._num_players: int = None
        self._game_server_init()

    @staticmethod
    def _get_int(msg, low: int = -100, high: int = 100) -> int:
        while True:
            try:
                x = int(input(msg))
                if low <= x <= high:
                    break
                else:
                    print(f'Value should be between {low} and {high}. Please try again: ')
            except ValueError:
                print(f'Invalid input, int expected. Please try again: ')
        return x

    def _game_server_init(self):
        self._num_players = self._get_int('Number of players: ')
        card_type = self._get_int('Card type:\n1) default\n2) subsets\n', 1, 2)
        if card_type == 1:
            self._card_type = 'default'
        else:
            self._card_type = 'subset'
        trick_type = self._get_int('Trick type:\n1) default\n2) early\n', 1, 2)
        if trick_type == 1:
            self._trick_type = 'default'
        else:
            self._trick_type = 'early'
        self._game_server = GameServer(self._num_players, 'default', self._card_type, self._trick_type)
        # TODO let user choose game_type, hand_type

    def run(self):
        while not self._game_server.state(self._player).is_finished():
            for p in range(self._num_players):
                state = self._game_server.state(self._player)
                print(f'Player {self._player}\'s turn')
                print(f'Cards in hand:')
                cards = state.get_hands()[0].cards()  # GameServer only returns 1 item in get_hands -> current player's hand
                for i, c in enumerate(cards):
                    print(f'{i + 1}) {c}')
                played = False
                while not played:
                    played_card_idx = self._get_int('Choose a card to play: ', 1, len(cards)) - 1
                    played_card = cards[played_card_idx]
                    if self._game_server.play(self._player, played_card_idx):
                        played = True
                        self.clear_screen()
                        print(f'Player {self._player} played: {played_card}')
                        self._player = (self._player + 1) % self._num_players
                    else:
                        print('Can\'t play this card. Please try again:')
            self._player = self._game_server.state(self._player).get_trick_started_by()
            print('===============================')
            print(f'This trick\'s winner is player {self._player}')
            print('===============================')
        print(f'WINNERS ARE: {self._game_server.game_winners()}')

    @staticmethod
    def clear_screen():
        for _ in range(30):
            print()

