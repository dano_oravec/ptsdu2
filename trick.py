from typing import Tuple, List
from card_interface import CardInterface
from trick_interface import TrickInterface


class Trick(TrickInterface):

    def __init__(self):
        self._cards: List[CardInterface] = []

    def play_as_lowest(self, c: CardInterface, curr_hand_cards: Tuple[CardInterface, ...]) -> bool:
        for hand_c in curr_hand_cards:
            if hand_c < c:
                return False  # Can't play this card as lowest, there is a lower card in the hand
        self._cards.append(c)
        return True

    def play_normal(self, c: CardInterface) -> bool:
        for other_c in self._cards:
            if other_c > c:
                return False  # Can't play this card as normal, there is a bigger card in the trick already
        self._cards.append(c)
        return True

    @staticmethod
    def _exists_higher(card: CardInterface, others: List[CardInterface]) -> bool:
        return any([o > card for o in others])

    def winner(self, trick_started_by: int, num_players: int) -> int:
        winner = 0
        for i, c in enumerate(self._cards):
            # Making this weird way because of possible partial ordering
            if not self._exists_higher(c, self._cards[:i]):
                winner = i
        return (winner + trick_started_by) % num_players

    def size(self) -> int:
        return len(self._cards)

    def cards(self) -> Tuple[CardInterface, ...]:
        return tuple(self._cards)

    def reset(self):
        self._cards = []

    def is_finished(self, num_players: int) -> bool:
        return len(self._cards) == num_players

