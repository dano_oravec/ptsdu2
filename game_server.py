from typing import List
from game_server_interface import GameServerInterface
from game_state import GameState
from dealer import Dealer


class GameServer(GameServerInterface):

    def __init__(self, num_players: int, game_type: str = 'default', card_type: str = 'default',
                 trick_type: str = 'default', hand_type: str = 'default'):
        self._num_players = num_players
        self._card_type = card_type
        self._trick_type = trick_type
        self._hand_type = hand_type
        self._dealer = Dealer(game_type, self._card_type)
        self._game = self._dealer.create_game(self._num_players, self._trick_type, self._hand_type)

    def play(self, player: int, card_num: int) -> bool:
        return self._game.play(player, card_num)

    def state(self, player: int) -> GameState:
        full = self._game.state()
        return GameState(
            full.get_num_players(),
            (full.get_hands()[player],),  # Only leave current player's hand
            full.get_trick(),
            full.get_trick_started_by(),
            full.is_finished(),
            all_players=False
        )

    def game_winners(self) -> List[int]:
        return self._game.game_winners()
