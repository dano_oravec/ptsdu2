from typing import List
from abc import ABC, abstractmethod
from game_state import GameState


class GameInterface(ABC):

    @abstractmethod
    def play(self, player: int, card_num: int) -> bool:
        pass

    @abstractmethod
    def is_finished(self) -> bool:
        pass

    @abstractmethod
    def state(self) -> GameState:
        pass

    @abstractmethod
    def game_winners(self) -> List[int]:
        pass
