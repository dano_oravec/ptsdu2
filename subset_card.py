from typing import Set
from card_interface import CardInterface


class SubsetCard(CardInterface):

    def __init__(self, value: Set[int]):
        self._value = value
        self._all = {1, 2, 3, 4, 5, 6, 7}
        if not self._value_valid(value):
            raise ValueError('Invalid card value')

    def string(self) -> str:
        return self.__str__()

    def get_value(self) -> object:
        return self._value.copy()

    def is_ge(self, other: 'CardInterface') -> bool:
        return self.__ge__(other)

    def comparable(self, other: CardInterface) -> bool:
        if not isinstance(other, SubsetCard):
            return False
        return self < other or self >= other

    def _value_valid(self, value: object) -> bool:
        if not isinstance(value, set):
            return False
        return self._value.issubset(self._all)

    def __lt__(self, other: 'CardInterface') -> bool:
        if not isinstance(other, SubsetCard):
            return NotImplemented
        other_val = other.get_value()
        if not isinstance(other_val, set):
            raise ValueError('Value of SubsetCard is not set, this should never happen.')
        return self._value.issubset(set(other_val)) and self._value != other_val

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SubsetCard):
            return NotImplemented
        other_val = other.get_value()
        if not isinstance(other_val, set):
            raise ValueError('Value of SubsetCard is not set, this should never happen.')
        return self._value == other_val

    def __ge__(self, other):
        return self > other or self == other

    def __str__(self):
        if self._value == set():
            return '{}'
        return str(self._value)
