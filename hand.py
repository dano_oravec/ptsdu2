from typing import List
from hand_interface import HandInterface
from card_interface import CardInterface
from trick_interface import TrickInterface


class Hand(HandInterface):

    def __init__(self, cards: List[CardInterface]):
        self._cards: List[CardInterface] = cards.copy()

    def cards(self) -> List[CardInterface]:
        return self._cards

    def play(self, card_num: int, trick: TrickInterface) -> bool:
        if card_num >= len(self._cards) or card_num < 0:
            return False  # Invalid card_num
        c = self._cards[card_num]
        #  Trick checks whether the play is possible, so we just have to try and see if it passes
        if trick.play_as_lowest(c, tuple(self._cards)) or trick.play_normal(c):
            del self._cards[card_num]
            return True
        return False

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Hand):
            return NotImplemented
        return sorted(self._cards, key=lambda x: str(x)) == sorted(other.cards(), key=lambda x: str(x))
