from typing import Tuple
from trick_interface import TrickInterface
from hand_interface import HandInterface


class GameState:

    def __init__(self, num_players: int, hands: Tuple[HandInterface, ...],
                 trick: TrickInterface, trick_started_by: int, is_finished: bool, all_players: bool = True):
        self._num_players: int = num_players
        self._hands: Tuple[HandInterface, ...] = hands
        if all_players and num_players != len(hands):
            raise ValueError('Too many or not enough players for provided hands')
        self._trick: TrickInterface = trick
        self._trick_started_by = trick_started_by
        self._is_finished = is_finished

    def get_num_players(self) -> int:
        return self._num_players

    def get_hands(self) -> Tuple[HandInterface, ...]:
        return self._hands

    # Get hand of the player whose turn it is
    def get_curr_hand(self):
        return self._hands[(self._trick_started_by + len(self._trick.cards())) % self._num_players]

    def get_trick(self) -> TrickInterface:
        return self._trick

    def get_trick_started_by(self) -> int:
        return self._trick_started_by

    def is_finished(self) -> bool:
        return self._is_finished
