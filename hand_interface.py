from typing import List
from abc import ABC, abstractmethod
from card_interface import CardInterface
from trick_interface import TrickInterface


class HandInterface(ABC):

    @abstractmethod
    def cards(self) -> List[CardInterface]:
        pass

    @abstractmethod
    def play(self, card_num: int, trick: TrickInterface) -> bool:
        pass

    @abstractmethod
    def __eq__(self, other):
        pass
