from card_interface import CardInterface


class Card(CardInterface):

    def __init__(self, value: str):
        self._value: str = value
        self._ordering = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A')
        if not self._value_valid(value):
            raise ValueError('Invalid card value')

    def string(self) -> str:
        return self.__str__()

    def is_ge(self, other: 'CardInterface') -> bool:
        return self.__ge__(other)

    def get_value(self) -> object:
        return self._value

    def _value_valid(self, value: object) -> bool:
        if not isinstance(value, str):
            return False
        return value in self._ordering

    def comparable(self, other: 'CardInterface') -> bool:
        if not isinstance(other, Card):
            return False
        return True

    def __lt__(self, other: 'CardInterface') -> bool:
        return not self.is_ge(other)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Card):
            return NotImplemented
        return self._value == other.string()

    def __ge__(self, other):
        return self._ordering.index(self._value) >= self._ordering.index(other.string())

    def __str__(self):
        return self._value
