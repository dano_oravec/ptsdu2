from typing import List
from game import Game
from game_interface import GameInterface
from trick import Trick
from early_trick import EarlyTrick
from trick_interface import TrickInterface
from hand import Hand
from card_interface import CardInterface
from card import Card
from subset_card import SubsetCard
from random import shuffle


class Dealer:

    def __init__(self, game_type: str = 'default', card_type: str = 'default', cards_per_player: int = 6):
        self._game_type = game_type
        self._card_type = card_type
        self._cards_per_player = cards_per_player

    def _all_cards(self) -> List[CardInterface]:
        if self._card_type == 'subset':
            cards: List[CardInterface] = []
            for i in range(1 << 7):
                curr = []
                for j in range(7):
                    if i & (1 << j):
                        curr.append(j + 1)
                cards.append(SubsetCard(set(curr)))
            shuffle(cards)
            return cards
        if self._card_type != 'default':
            raise ValueError('Unknown card_type')
        vals = sorted(['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'] * 4)
        shuffle(vals)
        return list(map(lambda val: Card(value=val), vals))

    def _deal(self, num_players: int) -> List[List[CardInterface]]:
        hands_cards: List[List[CardInterface]] = [[] for _ in range(num_players)]
        cards = self._all_cards()  # Get shuffled cards, then give first segment to first player, second to second, ...
        for i in range(num_players):
            left = i * self._cards_per_player
            right = left + self._cards_per_player
            hands_cards[i] = cards[left:right]
        return hands_cards

    def create_game(self, num_players: int, trick_type: str = 'default', hand_type: str = 'default') -> GameInterface:
        if self._game_type == 'default' and hand_type == 'default':
            hands_cards = self._deal(num_players)
            trick: TrickInterface = Trick()
            if trick_type == 'early':
                trick = EarlyTrick()
            elif trick_type != 'default':
                raise ValueError('Unknown trick_type')
            return Game(num_players, trick, [Hand(hands_cards[i]) for i in range(num_players)])
        raise ValueError('Unknown game_type, trick_type or hand_type')

    def game_type(self) -> str:
        return self._game_type
