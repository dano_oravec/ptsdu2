from typing import List
from game_interface import GameInterface
from game_state import GameState
from trick_interface import TrickInterface
from hand_interface import HandInterface
from card_interface import CardInterface


class Game(GameInterface):

    def __init__(self, num_players: int, trick, hands):
        self._num_players = num_players
        self._trick_started_by: int = 0
        self._trick: TrickInterface = trick
        self._hands: List[HandInterface] = hands
        self._last_winner: int = -1

    def play(self, player: int, card_num: int) -> bool:
        if self._hands[player].play(card_num, self._trick):
            if self._trick.is_finished(self._num_players):
                # This can be used by frontend to determine trick winner
                self._trick_started_by = self._trick.winner(self._trick_started_by, self._num_players)
                self._trick.reset()
            return True
        return False

    def is_finished(self) -> bool:
        finished = True
        for h in self._hands:
            if len(h.cards()) > 1:
                finished = False
        return finished

    def state(self) -> GameState:
        return GameState(self._num_players, tuple(self._hands), self._trick, self._trick_started_by, self.is_finished())

    @staticmethod
    def _exists_higher(card: CardInterface, others: List[CardInterface]):
        return any([o > card for o in others])

    def game_winners(self) -> List[int]:
        if not self.is_finished():
            raise RuntimeError('There is not a winner yet')
        win_cards: List[CardInterface] = []
        for h in self._hands:
            c = h.cards()[0]  # There is exactly 1 card in the hand, because _is_finished is True
            new_win_cards: List[CardInterface] = []
            for other in win_cards:
                if not other < c:
                    new_win_cards.append(other)
            if not self._exists_higher(c, win_cards):
                new_win_cards.append(c)
            win_cards = new_win_cards
        winners: List[int] = []
        for i, h in enumerate(self._hands):
            c = h.cards()[0]
            if c in win_cards:
                winners.append(i)
        return winners
