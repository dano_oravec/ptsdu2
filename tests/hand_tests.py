import random
from typing import Tuple, List
from tests.printer import Printer
from tests.test import Test
from card_interface import CardInterface
from trick_interface import TrickInterface
from hand import Hand


class MockedCard(CardInterface):

    def __init__(self, value: str):
        self._value: str = value

    def string(self) -> str:
        return self._value

    def _value_valid(self, value: object) -> bool:
        return True

    def is_ge(self, other: 'CardInterface') -> bool:
        return self.__ge__(other)

    def get_value(self) -> object:
        return self._value

    def comparable(self, other: 'CardInterface') -> bool:
        return True

    def __ge__(self, other: 'CardInterface') -> bool:
        return self._value > other.string()

    def __lt__(self, other: 'CardInterface') -> bool:
        return self._value < other.string()

    def __eq__(self, other: object) -> bool:
        if not isinstance(object, MockedCard):
            return NotImplemented
        return self._value == other.string()

    def __str__(self) -> str:
        return self._value


class MockedTrick(TrickInterface):

    def __init__(self):
        self._cards: List[CardInterface] = []
        self._rng = random.Random(1)

    def play_as_lowest(self, c: CardInterface, curr_hand_cards: Tuple[CardInterface, ...]) -> bool:
        return self.play_normal(c)

    def play_normal(self, c: CardInterface) -> bool:
        allow = bool(self._rng.randint(0, 1))
        if allow:
            self._cards.append(c)
            return True
        return False

    def winner(self, trick_started_by: int, num_players: int) -> int:
        return 0

    def size(self) -> int:
        return len(self._cards)

    def cards(self) -> Tuple[CardInterface, ...]:
        return tuple(self._cards)

    def reset(self):
        self._cards = []

    def is_finished(self, num_players: int) -> bool:
        return len(self._cards) == num_players


class HandTests(Test):

    def __init__(self):
        self._p = Printer('Hand')
        self._rng = random.Random(1)

    def _gen_random_cards(self, amt: int) -> List[CardInterface]:
        cards: List[CardInterface] = []
        for _ in range(amt):
            value = chr(ord('A') + self._rng.randrange(0, 26))
            cards.append(MockedCard(value))
        return cards

    def test_cards(self) -> bool:
        amt = self._rng.randint(1, 10)
        cards = self._gen_random_cards(amt)
        hand = Hand(cards)
        return hand.cards() == cards

    def test_cards_with_play(self) -> bool:
        amt = self._rng.randint(1, 10)
        cards = sorted(self._gen_random_cards(amt))
        hand = Hand(cards)
        trick = MockedTrick()
        play_attempts = self._rng.randint(1, amt)
        for _ in range(play_attempts):
            card_to_play_idx = self._rng.randrange(0, len(cards))
            if hand.play(card_to_play_idx, trick):
                del cards[card_to_play_idx]
        return cards == hand.cards()

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_cards,
            self.test_cards_with_play
        ]
        for t in tests:
            self._rng.seed(1)
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

