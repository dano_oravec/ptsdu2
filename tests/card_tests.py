import random
from tests.printer import Printer
from tests.test import Test
from card import Card


class CardTests(Test):

    def __init__(self):
        self._p = Printer('Card')
        self._rng = random.Random(1)
        self._ordering = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A')

    def get_valid_value(self):
        return self._rng.choice(self._ordering)

    def test_creation(self):
        try:
            value = self.get_valid_value()
            c = Card(value=value)
        except Exception:
            return False
        return True

    def test_invalid_values(self):
        try:
            get_value = lambda: ord('A') + self._rng.randint(0, 50)
            bad_value = get_value()
            while bad_value in self._ordering:
                bad_value = get_value()
            c = Card(value=bad_value)
        except ValueError:
            return True
        return False

    def test_string(self):
        value = self.get_valid_value()
        c = Card(value=value)
        return c.string() == value

    def test_is_ge(self):
        mx_idx = len(self._ordering)-1
        l_idx = self._rng.randint(0, mx_idx)
        geq_idx = self._rng.randint(l_idx, mx_idx)
        l_card = Card(value=self._ordering[l_idx],)
        geq_card = Card(value=self._ordering[geq_idx])
        if not geq_card.is_ge(l_card):
            return False
        if l_idx < geq_idx:
            if l_card.is_ge(geq_card):
                return False
        elif not l_card.is_ge(geq_card):
            return False
        return True

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_creation,
            self.test_invalid_values,
            self.test_string,
            self.test_is_ge
        ]
        for t in tests:
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

