import random
from typing import Set
from tests.printer import Printer
from tests.test import Test
from subset_card import SubsetCard


class SubsetCardTests(Test):

    def __init__(self):
        self._p = Printer('SubsetCard')
        self._rng = random.Random(1)

    def _get_valid_value(self) -> Set[int]:
        mask = self._rng.randrange(0, 1 << 7)
        subset: Set[int] = set()
        for i in range(7):
            if mask & (1 << i):
                subset.add(i + 1)
        return subset

    @staticmethod
    def _is_valid(value: Set[int]) -> bool:
        val = value.copy()
        for i in range(1, 7 + 1):
            if i in val:
                val.remove(i)
        return len(val) == 0

    def test_creation(self) -> bool:
        try:
            value = self._get_valid_value()
            c = SubsetCard(value=value)
        except Exception:
            return False
        return True

    def test_invalid_values(self):
        try:
            get_value = lambda: self._rng.sample({1, 2, 3, 4, 5, 6, 7, 8, 9}, 7)
            bad_value = get_value()
            while self._is_valid(bad_value):
                bad_value = get_value()
            c = SubsetCard(value=bad_value)
        except ValueError:
            return True
        return False

    def test_string(self):
        value = self._get_valid_value()
        c = SubsetCard(value=value)
        if len(value) == 0:
            return c.string() == '{}'
        return c.string() == str(value)

    def test_is_ge(self):
        geq_val = self._get_valid_value()
        l_val = geq_val.copy()
        for i in range(len(geq_val)):
            remove = self._rng.randint(1, 3)
            if remove == 1:
                l_val.remove(list(geq_val)[i])
        l_card = SubsetCard(l_val)
        geq_card = SubsetCard(geq_val)
        if not geq_card.is_ge(l_card):
            return False
        if l_val == geq_val:
            if not l_card.is_ge(geq_card):
                return False
        elif l_card.is_ge(geq_card):
            return False
        return True

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_creation,
            self.test_invalid_values,
            self.test_string,
            self.test_is_ge
        ]
        for t in tests:
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

