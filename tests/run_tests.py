from tests.card_tests import CardTests
from tests.trick_tests import TrickTests
from tests.hand_tests import HandTests
from tests.game_dealer_tests import GameDealerTests
from tests.game_server_tests import GameServerTests
from tests.early_trick_tests import EarlyTrickTests
from tests.subset_card_tests import SubsetCardTests

test_cases = 100

testing = [
    CardTests(),
    TrickTests(),
    HandTests(),
    GameDealerTests(),
    GameServerTests(),
    EarlyTrickTests(),
    SubsetCardTests()
]

for t in testing:
    t.run_all(test_cases=test_cases)
    print('-------------------------')
