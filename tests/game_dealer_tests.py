import random
from typing import List
from tests.printer import Printer
from tests.test import Test
from dealer import Dealer
from game import Game
from trick import Trick
from hand import Hand
from card import Card
from card_interface import CardInterface


class GameDealerTests(Test):

    def __init__(self):
        self._p = Printer('Game,Dealer')
        self._rng = random.Random(1)
        self._num_players = self._rng.randint(2, 10)

    @staticmethod
    def _get_random_card() -> Card:
        ordering = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A')
        return Card(random.choice(list(ordering)))

    def test_play_state(self) -> bool:
        d = Dealer()
        g = d.create_game(self._num_players)
        try:
            player0_cards_before = g.state().get_hands()[0].cards()
            played_card = player0_cards_before[0]
            cnt_before = player0_cards_before.count(played_card)
            assert(cnt_before > 0)
            g.play(0, 0)
            player0_cards_after = g.state().get_hands()[0].cards()
            cnt_after = player0_cards_after.count(played_card)
            assert(cnt_after == cnt_before - 1)
        except AssertionError:
            return False
        return True

    def test_game_type(self) -> bool:
        game_type = chr(ord('A') + self._rng.randrange(0, 26))
        d = Dealer(game_type)
        if d.game_type() == game_type:
            return True
        return False

    @staticmethod
    def _exists_higher(card: CardInterface, others: List[CardInterface]):
        return any([o > card for o in others])

    def test_game_winners(self) -> bool:
        cards: List[CardInterface] = [self._get_random_card() for _ in range(self._num_players)]
        winners = []
        for i in range(self._num_players):
            if not self._exists_higher(cards[i], cards):
                winners.append(i)
        g = Game(self._num_players, Trick(), [Hand([cards[i]]) for i in range(self._num_players)])
        return sorted(winners) == sorted(g.game_winners())

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_play_state,
            self.test_game_type,
            self.test_game_winners
        ]
        for t in tests:
            self._rng.seed(1)
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

