import random
from typing import List
from tests.printer import Printer
from tests.test import Test
from card_interface import CardInterface
from card import Card
from early_trick import EarlyTrick


# Sociable tests, we will use Card class for cards
class EarlyTrickTests(Test):

    def __init__(self):
        self._p = Printer('EarlyTrick')
        self._rng = random.Random(1)
        self._num_players = self._rng.randint(2, 10)
        self._default_ordering = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A')

    def _get_random_card(self) -> CardInterface:
        return Card(random.choice(self._default_ordering))

    def _gen_hands(self, hand_size: int) -> List[List[CardInterface]]:
        players_cards: List[List[CardInterface]] = [[] for _ in range(self._num_players)]
        for i in range(self._num_players):
            for j in range(hand_size):
                players_cards[i].append(self._get_random_card())
            players_cards[i].sort()
        return players_cards

    def test_reset(self) -> bool:
        trick = EarlyTrick()
        for _ in range(self._num_players):
            trick._cards.append(self._get_random_card())
        trick.reset()
        return not len(trick._cards)  # Must have 0 cards after reset to pass the test

    def test_cards(self) -> bool:
        trick = EarlyTrick()
        cards = []
        for _ in range(self._num_players):
            cards.append(self._get_random_card())
        cards.sort()
        amt_cards_played = self._rng.randint(0, len(cards))
        played_cards = []
        for i in range(amt_cards_played):
            trick.play_normal(cards[i])
            played_cards.append(cards[i])
        return trick.cards() == tuple(played_cards)

    def test_size(self) -> bool:
        trick = EarlyTrick()
        amt_cards = self._rng.randint(1, 100)
        last_played = None
        sz = 0
        # Some cards won't get played, because there has been a bigger card played already in the trick
        for _ in range(amt_cards):
            c = self._get_random_card()
            trick.play_normal(c)
            if last_played is None or last_played <= c:
                last_played = c
                sz += 1
        return sz == trick.size()

    def test_winner(self) -> bool:
        trick = EarlyTrick()
        winning_card, winner = None, -1
        for i in range(self._num_players):
            c = self._get_random_card()
            trick.play_as_lowest(c, (c,))
            if winning_card is None or c > winning_card:
                winning_card = c
                winner = i
        assert(winner != -1)
        return winner == trick.winner(0, self._num_players)

    def test_play_normal(self) -> bool:
        trick = EarlyTrick()
        players_cards = self._gen_hands(self._rng.randint(1, 6))
        winning_card = None
        for i in range(self._num_players):
            card_to_play_idx = self._rng.randrange(0, len(players_cards[i]))
            card_to_play = players_cards[i][card_to_play_idx]
            if winning_card is not None and card_to_play < winning_card:
                if trick.play_normal(card_to_play):
                    return False
            else:
                if not trick.play_normal(card_to_play):
                    return False
                if winning_card is None or card_to_play > winning_card:
                    winning_card = card_to_play
        return True

    def test_play_as_lowest(self) -> bool:
        trick = EarlyTrick()
        players_cards = self._gen_hands(self._rng.randint(1, 6))
        for i in range(self._num_players):
            card_to_play_idx = self._rng.randrange(0, len(players_cards[i]))
            card_to_play = players_cards[i][card_to_play_idx]
            if card_to_play == players_cards[i][0]:
                if not trick.play_as_lowest(card_to_play, tuple(players_cards[i])):
                    return False
            else:
                if trick.play_as_lowest(card_to_play, tuple(players_cards[i])):
                    return False
        return True

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_reset,
            self.test_cards,
            self.test_size,
            self.test_winner,
            self.test_play_normal,
            self.test_play_as_lowest
        ]
        for t in tests:
            self._rng.seed(1)
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

