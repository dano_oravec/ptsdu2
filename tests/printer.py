class Printer:

    def __init__(self, cls_name: str):
        self._cls_name = cls_name
        self.GREEN = '\033[92m'
        self.FAIL = '\033[91m'
        self.END = '\033[0m'

    def fail(self, msg):
        print(f'{self.FAIL}[FAILED] {self._cls_name}::{msg}{self.END}')

    def success(self, msg):
        print(f'{self.GREEN}[PASSED] {self._cls_name}::{msg}{self.END}')
