from abc import ABC, abstractmethod


class Test(ABC):

    @abstractmethod
    def run_all(self, test_cases: int = 10):
        pass
