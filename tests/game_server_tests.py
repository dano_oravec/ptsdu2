import random
from tests.printer import Printer
from tests.test import Test
from game_server import GameServer


class GameServerTests(Test):

    def __init__(self):
        self._p = Printer('GameServer')
        self._rng = random.Random(1)
        self._num_players = self._rng.randint(2, 10)

    def test_play(self):
        try:
            gs = GameServer(self._num_players)
            return True  # GameServer::play is only 'alias' for Game::play which is tested in game_server_tests.py
        except:
            return False

    def test_state(self):
        try:
            gs = GameServer(self._num_players)
            full_state = gs._game.state()
            player = self._rng.randrange(0, self._num_players)
            desired_hand = full_state.get_hands()[player]
            got_hands = gs.state(player).get_hands()
            return len(got_hands) == 1 and desired_hand == got_hands[0]
        except:
            return False

    def test_game_winners(self):
        try:
            gs = GameServer(self._num_players)
            return True  # GameServer::game_winners is only 'alias' for Game::game_winners which is tested in game_server_tests.py
        except:
            return False

    def run_all(self, test_cases: int = 10):
        tests = [
            self.test_play,
            self.test_state,
            self.test_game_winners
        ]
        for t in tests:
            self._rng.seed(1)
            passing = True
            for tc in range(test_cases):
                passing = passing and t()
            if passing:
                self._p.success(t.__name__)
            else:
                self._p.fail(t.__name__)

